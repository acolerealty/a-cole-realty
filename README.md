In October 2013, Angie started her own company, A Cole Realty, and began providing exceptional service for buyers and sellers alike in the Triangle area. A Cole Realty has quickly grown and has had the pleasure of assisting over 300 clients in 2019. Angie focuses strictly on helping sellers list and sell their homes while managing her team.

Website : https://www.acolerealty.com/